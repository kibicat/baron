// 🎩🐈 Baron ∷ dev-deps.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { xhtmlNamespace } from "./names.js";
import {
  DOMImplementation,
  DOMParser,
  XMLSerializer,
} from "https://esm.sh/@xmldom/xmldom@0.8.2";

{ // Set up “window”.
  const document = new DOMImplementation().createDocument(
    xhtmlNamespace,
    "html",
    null,
  );
  const { documentElement } = document;
  documentElement.appendChild(
    document.createElementNS(xhtmlNamespace, "head"),
  );
  documentElement.appendChild(
    document.createElementNS(xhtmlNamespace, "body"),
  );
  Object.defineProperties(globalThis, {
    document: {
      configurable: false,
      enumerable: true,
      value: document,
      writable: false,
    },
    window: {
      configurable: false,
      enumerable: true,
      value: globalThis,
      writable: false,
    },
  });
}

{ // Polyfill `element.append()`.
  Object.getPrototypeOf(
    document.documentElement,
  ).append = function append(...children) {
    for (const child of children) {
      this.appendChild(
        typeof child === "string"
          ? this.ownerDocument.createTextNode(child)
          : child,
      );
    }
  };
}

{ // “Polyfill” custom elements.
  globalThis.HTMLElement = class HTMLElement {};
  globalThis.customElements = { define: () => {} };
}

{ // Polyfill `FinalizationRegistry`.
  const registries = new Set();

  globalThis.FinalizationRegistry = class FinalizationRegistry {
    #handler;
    #registry = new Map();

    constructor(handler) {
      this.#handler = handler;
      registries.add(this);
    }

    register(object, heldValue, token) {
      this.#registry.set(token ?? object, { object, heldValue });
    }

    unregister(token) {
      this.#registry.delete(token);
    }

    "🥸finalize🥸"(token) {
      if (this.#registry.has(token)) {
        this.#handler(this.#registry.get(token)?.heldValue);
      }
    }

    get [Symbol.toStringTag]() {
      return "FinalizationRegistry";
    }

    /**
     * Mocks finalization of object corresponding to token for garbage
     * collection.
     */
    static "🥸finalize🥸"(token) {
      for (const registry of registries) {
        registry["🥸finalize🥸"](token);
      }
    }
  };
}

export { DOMImplementation, DOMParser, XMLSerializer };

export {
  assert,
  assertEquals,
  assertNotEquals,
  assertStrictEquals,
  assertThrows,
  unimplemented,
  unreachable,
} from "https://deno.land/std@0.134.0/testing/asserts.ts";
