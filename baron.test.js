// 🎩🐈 Baron ∷ baron.test.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

// Assure Baron loads after dependencies (so the
// FinalizationRegistry polyfill is definitely in place).
//
// Because of the dynamic import, this requires `--allow-read` and
// `--allow-net` (for dependencies) to run in Deno.
const {
  assert,
  assertStrictEquals,
  Baron,
} = await import("./dev-deps.js").then(
  ({ assert, assertStrictEquals }) =>
    import("./baron.js").then(
      ({ default: Baron }) => ({ assert, assertStrictEquals, Baron }),
    ),
);

const counts = {
  initialization: 0,
  initializedCallback: 0,
  deinitialization: 0,
};

Baron.initializers.test = function () {
  this.heldValues.test = counts;
  this.register(Baron.initialized, () => ++counts.initializedCallback);
  ++counts.initialization;
};
Baron.deinitializers.test = function () {
  ++this.deinitialization;
};

Deno.test({
  name: "Initializers run when a Baron instance is created.",
  fn: () => {
    counts.initialization = 0;
    counts.initializedCallback = 0;
    assertStrictEquals(counts.initialization, 0);
    assertStrictEquals(counts.initializedCallback, 0);
    new Baron();
    assertStrictEquals(counts.initialization, 1);
    assertStrictEquals(counts.initializedCallback, 1);
    new Baron();
    assertStrictEquals(counts.initialization, 2);
    assertStrictEquals(counts.initializedCallback, 2);
  },
});

Deno.test({
  name: "Callbacks can be registered and unregistered.",
  fn: () => {
    // Setup.
    const key = Symbol();
    const { dispatch, register, unregister } = Baron.prototype;
    const object = { count: 0 };
    const callback = function ({ n }) {
      this.count += n;
    };

    // Registration.
    register.call(object, key, callback);
    assertStrictEquals(object.count, 0);
    dispatch.call(object, key, { n: 3 });
    assertStrictEquals(object.count, 3);

    // Unregistration.
    unregister.call(object, key, callback);
    assertStrictEquals(object.count, 3);
    dispatch.call(object, key, { n: 2 });
    assertStrictEquals(object.count, 3);
  },
});

Deno.test({
  name:
    "Deinitializers run on finalization and are passed appropriate held values.",
  fn: () => {
    const baron = new Baron();
    counts.deinitialization = 0;
    assertStrictEquals(counts.deinitialization, 0);
    FinalizationRegistry["🥸finalize🥸"](baron);
    assertStrictEquals(counts.deinitialization, 1);
  },
});

Deno.test({
  name: "Baron.initialize works.",
  fn: () => {
    const foo = Symbol();
    const bar = Symbol();
    Baron.initialize({ [foo]: bar });
    assert(document[Baron.symbol] !== undefined);
    assertStrictEquals(
      document[Baron.symbol].context.document,
      document,
    );
    assertStrictEquals(document[Baron.symbol].context[foo], bar);
  },
});

Deno.test({
  name: "Baron::close can’t be made to deinitialize twice.",
  fn: () => {
    const baron = new Baron();
    counts.deinitialization = 0;
    assertStrictEquals(counts.deinitialization, 0);
    baron.close();
    assertStrictEquals(counts.deinitialization, 1);
    baron.close();
    assertStrictEquals(counts.deinitialization, 1);
    FinalizationRegistry["🥸finalize🥸"](baron);
    assertStrictEquals(counts.deinitialization, 1);
  },
});
