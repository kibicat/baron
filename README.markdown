# 🎩🐈&#xA0;Baron

🎩🐈&#xA0;Baron is an application engine intended to serve as the
“backend” for web applications like [🌑🐈&#xA0;Lune][Lune]. Its core
features are described below.

## Features

### Global availability

It is expected that most applications will route their behaviours
through a single `Baron` instance located at `document[Baron.symbol]`,
and this module provides special functionality for this case. You can
easily set up this object using the `Baron.initialize` static method.

### Events

The `Baron` prototype provides three methods which can be used to
support an event‐based application model :—

- `Baron::dispatch(eventKey, eventData)` fires an event named
  `eventKey` at the object it is called on, passing a frozen copy of
  the provided `eventData`, or `null` if `eventData` is `null`.

  `eventKey` will be coerced into a property key (either a symbol or
  string). Using symbols for `eventKey` is recommended to prevent
  conflicts. `eventData` has no default value; passing an `undefined`
  value will throw an error.

- `Baron::register(eventKey, callbackFn)` registers a `callbackFn` to
  be called when an event named `eventKey` fires at the object it is
  called on. Callback functions can only be registered once, and are
  called in order of registeration.

  The object receiving the event is provided as the `this` value of the
  callback function, and its first argument will be either `null` or a
  frozen object containing the event data.

- `Baron::unregister(eventKey, callbackFn)` unregisters a `callbackFn`
  function from being called when an event named `eventKey` fires at
  the object it is called on. If no such callback function has been
  registered, this method does nothing.

These methods are intentionally generic: Although they are provided by
the `Baron` prototype, they can be called on any object. However, it is
expected that listening or calling events on the “global” `Baron`
instance at `document[Baron.symbol]` will be sufficient for most
applications.

### Lifecycle callbacks

The lifecycle of Baron instances can be managed through initialization
and deinitialization callbacks, which can be set on
`Baron.initializers` and `Baron.deinitializers` as enumerable own
properties prior to instance construction.

It is often useful to be able to guarantee that a given initializer was
indeed called on a `Baron` instance during its construction. There are
two possible approaches a library could take to enable this :—

1. It could make a fresh copy of the initializers used for every
   instance, or

2. It could ensure every instance uses the same initializers.

This module takes the second approach: Once a `Baron` instance is
constructed, the values of `Baron.initializers` and
`Baron.deinitializers` are effectively frozen. Consequently, it is
advised that you defer `Baron` instance construction until after all
scripts have had a chance to load, for example by waiting for the
`DOMContentLoaded` D·O·M event.

> **☡ Warning:** Initializers and deinitializers _can_ be defined as
> accessor properties, the values of which _can_ change over time. Use
> `Object.getOwnPropertyDescriptor(Baron.initializers, myKey)?.value`
> if you need to be sure that an initializer was present as a data
> property when a `Baron` instance was constructed.

Initializers are called with the `Baron` instance as their `this`
value. Each `Baron` instance also has a frozen `context` object with at
least a `document` property; the intent is that this object is used to
specify initialization parameters.

Because deinitializers are called when a `Baron` instance is being
finalized for garbage collection, they cannot have a reference to the
`Baron` instance in question. Instead, they are passed the value of the
corresponding property on the instance’s `heldValues` object.

## Custom Elements

Custom elements may be scheduled for registration by assigning them as
values to the `Baron[Baron.customElements]` object, with a key which is
a valid HTML custom element name. The advantage to using this object
instead of manually registering the custom element yourself is that it
defers registration until the first `Baron` instance is constructed,
enabling future modules to replace your definition with their own.

Suppose, for example, you write a `FavouriteFlagElement` with the
following definition :—

```js
class FavouriteFlagElement extends HTMLElement {
  constructor (flag = "🏴‍☠️") {
    super();
    this.attachShadow({ mode: "open" });
    this.shadowRoot.append(flag);
  }
}

Baron[Baron.customElements].favourite-flag = FavouriteFlagElement;
```

Another module might modify this functionality like follows :—

```js
class MyFavouriteFlagElement extends FavouriteFlagElement {
  constructor () {
    super("🏳️‍⚧️");
  }
}

Baron[Baron.customElements].favourite-flag = MyFavouriteFlagElement;
```

Now, every instance of `<favourite-flag>` will display `🏳️‍⚧️` instead
of `🏴‍☠️`. This kind of replacement would not be possible if
`FavouriteFlagElement` had been registered to the custom elements
registry directly.

If a custom element extends an HTML element, this can be specified with
a `Baron.customElementExtends` static property on the element
definition.

## License

Source files are licensed under the terms of the <cite>Mozilla Public
License, version 2.0</cite>. For more information, see
[LICENSE](./LICENSE).

[Lune]: <https://gitlab.com/kibicat/lune>
