// 🎩🐈 Baron ∷ custom-elements.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import Baron from "./baron.js";

// This module defines a new initializer on `Baron` which iterates over
// the enumerable own properties of the `[Baron.customElements]` static
// property on `Baron` and, for each property which is a valid custom
// element name, registers it to the custom element registry (provided
// one exists in the current context).
//
// By default, autonomous custom elements are defined. If a customized
// built‐in element is intended, a static
// `[Baron.customElementExtends]` property must be defined on the
// custom element definition to give the local name of the element
// being extended.
//
// This initializer runs only once, when the first Baron instance is
// constructed. After this point, the value of
// `Baron[Baron.customElements]` is frozen.

const customElementsSymbol = Symbol("Baron.customElements");

let processedCustomElements = false;

Object.defineProperties(
  Baron,
  {
    customElementExtends: {
      configurable: false,
      enumerable: true,
      value: Symbol("Baron.customElementExtends"),
      writable: false,
    },
    customElements: {
      configurable: false,
      enumerable: true,
      value: customElementsSymbol,
      writable: false,
    },
    [customElementsSymbol]: {
      configurable: false,
      enumerable: true,
      value: {},
      writable: true,
    },
  },
);

Baron.initializers[customElementsSymbol] = () => {
  if (processedCustomElements) {
    // The custom elements have already been processed.
    return;
  } else {
    // The custom elements have not yet been processed.
    processedCustomElements = true; // don’t reprocess even if error
    const customElements = Object.freeze({
      ...new Object(Baron[customElementsSymbol]),
    });
    Object.defineProperty(Baron, customElementsSymbol, {
      value: customElements,
      writable: false,
    });
    if (!("customElements" in globalThis)) {
      // No custom element registry is available.
      return;
    } else {
      // A custom element registry exists on the global object.
      for (
        const [elementName, elementDefinition] of Object.entries(
          customElements,
        )
      ) {
        if (
          !/^[a-z][.0-9_a-z\u{B7}\u{C0}-\u{D6}\u{D8}-\u{F6}\u{F8}-\u{37D}\u{37F}-\u{1FFF}\u{200C}-\u{200D}\u{203F}-\u{2040}\u{2070}-\u{218F}\u{2C00}-\u{2FEF}\u{3001}-\u{D7FF}\u{F900}-\u{FDCF}\u{FDF0}-\u{FFFD}\u{10000}-\u{EFFFF}]*-[-.0-9_a-z\u{B7}\u{C0}-\u{D6}\u{D8}-\u{F6}\u{F8}-\u{37D}\u{37F}-\u{1FFF}\u{200C}-\u{200D}\u{203F}-\u{2040}\u{2070}-\u{218F}\u{2C00}-\u{2FEF}\u{3001}-\u{D7FF}\u{F900}-\u{FDCF}\u{FDF0}-\u{FFFD}\u{10000}-\u{EFFFF}]*$/u
            .test(elementName)
        ) {
          // The current element name is not a valid custom element name.
          /* do nothing */
        } else {
          // The current element name is a valid custom element name.
          globalThis.customElements.define(
            elementName,
            elementDefinition,
            {
              extends: elementDefinition[Baron.customElementExtends] ??
                null,
            },
          );
        }
      }
    }
  }
};
