// 🎩🐈 Baron ∷ baron.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { frozenCopy, isObject, toPropertyKey } from "./deps.js";

/**
 * Call the Baron deinitializers with their corresponding held values.
 *
 * See [`FinalizationRegistry` on the M·D·N][FinalizationRegistry] for
 * more on the shape and purpose of this callback.
 *
 * If a held value is marked as configurable, it will be removed. For
 * held values which should not be removed on deinitialization, mark
 * the corresponding property as nonconfigurable.
 *
 * [FinalizationRegistry]: <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/FinalizationRegistry>
 */
const $deinitialize = (heldValues) => {
  const { deinitializers } = $lifecycleCallbacks;
  for (const property of Reflect.ownKeys(deinitializers)) {
    deinitializers[property].call(heldValues[property]);
    if (
      Object.getOwnPropertyDescriptor(heldValues, property)
        ?.configurable
    ) {
      // The corresponding held value can be removed.
      delete heldValues[property];
    } else {
      // The corresponding held value was marked as nonconfigurable.
      /* do nothing */
    }
  }
};

/**
 * An object whose `initializers` and `deinitializers` properties
 * access and freeze the corresponding properties on `Baron`.
 */
const $lifecycleCallbacks = Object.create(
  null,
  {
    initializers: {
      configurable: true,
      enumerable: true,
      get() {
        const value = frozenCopy(Baron.initializers, null);
        const desc = {
          configurable: false,
          enumerable: true,
          value,
          writable: false,
        };
        Object.defineProperty(Baron, "initializers", desc);
        Object.defineProperty(
          $lifecycleCallbacks,
          "initializers",
          desc,
        );
        return value;
      },
    },
    deinitializers: {
      configurable: true,
      enumerable: true,
      get() {
        const value = frozenCopy(Baron.deinitializers, null);
        const desc = {
          configurable: false,
          enumerable: true,
          value,
          writable: false,
        };
        Object.defineProperty(Baron, "deinitializers", desc);
        Object.defineProperty(
          $lifecycleCallbacks,
          "deinitializers",
          desc,
        );
        return value;
      },
    },
  },
);

/**
 * A Baron application engine object.
 *
 * Baron instances provide a broad hook for interfacing with the
 * various components of a Baron application. Each document can have at
 * most one.
 */
export default class Baron {
  /** Whether this instance has been closed yet. */
  #closed = false;

  /** This is a brand check; see {@link Baron#baron}. */
  #isBaron;

  /**
   * Creates a new Baron instance using the provided context.
   *
   * The used context will be a frozen, shallow clone of the
   * enumerable own properties (including symbol properties) of the
   * provided one.
   *
   * The `document` property is required. Use `null` if you do not
   * intend for the instance to be associated with a document.
   */
  constructor(context = { document: globalThis.document ?? null }) {
    // Copy and freeze initializer and deinitializer objects.
    //
    // This leaves the original initializer and deinitializer objects
    // intact—and mutable—which might be a bit of a gotcha if you
    // have saved them to a variable instead of accessing their values
    // off of the Baron class directly.
    //
    // This will throw an error if `initializers` or `deinitializers`
    // have been reconfigured as accessor properties *and* made
    // nonconfigurable, which is desired behaviour.
    const { initializers, deinitializers: _ } = $lifecycleCallbacks;

    // Build the context from the enumerable own properties on the
    // provided object, according to the following rules :—
    //
    // - For data properties, create a nonconfigurable, nonwritable
    //   property with the same value.
    //
    // - For accessor properties, create a nonconfigurable accessor
    //   property with the same getter *and* setter.
    //
    // This is a compromise between wanting the initial context to be
    // frozen in stone and wanting to allow for contexts which might
    // change over time, and mimics the behaviour of `Object.freeze`.
    const finalContext = frozenCopy(context, null);

    // Verify (in D·O·M environments) that `context.document` is an
    // actual Document, if not null.
    //
    // While ordinarily this kind of strict typechecking is not ideal,
    // I would rather be able to make a strong guarantee about this to
    // initializers and other dependencies.
    //
    // (This check leverages the fact that both `document` on Window
    // and `location` on Document are defined as
    // `[LegacyUnforgeable]` in the H·T·M·L Standard.)
    const documentDescriptor = Object.getOwnPropertyDescriptor(
      finalContext,
      "document",
    );
    if ("get" in documentDescriptor || "set" in documentDescriptor) {
      // The document is specified with an accessor property.
      throw new TypeError(
        "Baron: The context document cannot be specified through an accessor property.",
      );
    } else {
      // The document is specified with a (nonconfigurable, readonly)
      // data property.
      const document = documentDescriptor.value;
      if (!isObject(document) && document !== null) {
        // The document is a primitive value other than null. Note that
        // while callable objects count as nonprimitive and
        // consequently will not throw this error, documents will never
        // be callable in the context of an ordinary D·O·M.
        throw new TypeError(
          "Baron: The context document must be an object or null.",
        );
      } else if (document == null) {
        // The document is null.
        /* do nothing */
      } else if (Object.hasOwn(globalThis, "document")) {
        // The current environment is D·O·M·like.
        try {
          Reflect.get(globalThis.document, "location", document);
        } catch {
          throw new TypeError(
            "Baron: The context document must implement the Document interface.",
          );
        }
      } else {
        // The current environment is not D·O·M·like, so no guarantees
        // about the nature of the context document can be made.
        /* do nothing */
      }
    }

    // Set up instance properties.
    //
    // The `context` property holds the context object created above.
    //
    // The `heldValues` property is an object to which held values can
    // be assigned, which will be passed to deinitializers should this
    // Baron instance be finalized for garbage collection.
    Object.defineProperties(this, {
      context: {
        configurable: false,
        enumerable: true,
        value: finalContext,
        writable: false,
      },
      heldValues: {
        configurable: false,
        enumerable: true,
        value: Object.create(null),
        writable: false,
      },
    });

    // Initialize and set up deinitialization. Initializers can grab
    // the context off of this, so there’s no need to pass it
    // separately.
    for (const property of Reflect.ownKeys(initializers)) {
      initializers[property].call(this);
    }
    baronFinalizationRegistry.register(this, this.heldValues, this);

    // Fire the `Baron.initialized` event.
    this.dispatch(Baron.initialized, null);
  }

  /**
   * Deinitializers for Baron instances.
   *
   * As with {@link Baron.initializers}, this object can be extended up
   * until the first Baron instance is constructed, at which point it
   * will be frozen.
   *
   * Deinitializers will be called with their `this` value set to the
   * value of the corresponding property in the instance’s `heldValues`
   * object. Obviously, this should not contain any strong references
   * to the instance itself, or else deinitialization won’t ever
   * happen.
   *
   * Deinitializers exist to allow for the removal of event listeners
   * and DOM observers once a Baron instance is no longer in use. It
   * is not expected that this will be commonly‐required functionality.
   * Try to write code which doesn’t require this sort of cleanup!
   *
   * It is **strongly recommended** that deinitializer keys be symbols
   * to prevent unintentional conflicts between extensions.
   */
  static deinitializers = Object.create(null);

  /**
   * Creates a new Baron instance with the provided context on the
   * `document` of the global object.
   *
   * Throws if the global object does not have a `document`.
   */
  static initialize(context) {
    if ("document" in globalThis) {
      // The global object has a `document`.
      const document = globalThis.document;
      const usedContext = Object.assign(
        Object.create(
          Object.getPrototypeOf(context),
          Object.getOwnPropertyDescriptors(context),
        ),
        { document },
      );
      return document[Baron.symbol] = new Baron(usedContext);
    } else {
      // The global object does not have a `document`.
      throw new TypeError(
        "Baron: Cannot initialize to a missing document.",
      );
    }
  }

  /** Indicates an initialization event. */
  static initialized = Symbol("Baron.initialized");

  /**
   * Initializers for Baron instances.
   *
   * This object can be extended (or replaced…) up until the first
   * Baron instance is constructed, at which point it will be frozen.
   *
   * Initializers are called as a part of Baron instance construction
   * with their `this` value set to the instance in question.
   *
   * It is **strongly recommended** that initializer keys be symbols to
   * prevent unintentional conflicts between extensions.
   */
  static initializers = Object.create(null);

  /** The Baron symbol. */
  static symbol = Symbol("Baron");

  /**
   * A brand check for Baron instances.
   *
   * Guarantees that this was properly constructed using the Baron
   * constructor. **Does not** guarantee that any particular
   * initializer was called, although this falls out as consequence of
   * them being frozen after the first invocation.
   *
   * Obviously, any object can override this method, so you need to
   * actually do `Reflect.get(Baron.prototype, "baron", $)` to be sure.
   * And you need to guarantee that no code has modified
   * `Baron.prototype` to be *really* sure.
   */
  get baron() {
    if (#isBaron in this) {
      return this;
    } else {
      throw new TypeError(
        "Baron: This was not constructed with the Baron constructor.",
      );
    }
  }

  /**
   * Deinitializes this Baron instance and unassociates it with its
   * document, unless it has already been closed.
   */
  close() {
    const document = this.context.document;
    if (!this.#closed) {
      // This has not yet been deinitialized.
      baronFinalizationRegistry.unregister(this);
      $deinitialize(this.heldValues);
      this.#closed = true;
    } else {
      // This has already been deinitialized.
      /* do nothing */
    }
    if (
      document !== null && baronsForDocuments.get(document) === this
    ) {
      // This is presently associated with its document.
      baronsForDocuments.delete(document);
    } else {
      // This is not presently associated with its document.
      /* do nothing */
    }
  }

  /**
   * Returns whether {@link Baron#close} has been called on this Baron
   * instance yet.
   */
  get closed() {
    return this.#closed;
  }

  /**
   * Dispatches the provided event data to the callbacks registered
   * with the provided event key, and returns this object.
   *
   * The provided event data is copied into a new, frozen object which
   * is provided to callbacks in order of registration.
   */
  dispatch(eventKey, eventData) {
    if (!isObject(this)) {
      // This is not an object.
      throw new TypeError(
        "Baron: Callbacks can only be dispatched on objects.",
      );
    } else {
      // This is an object.
      const registry = callbackRegistryForObject(this);
      const key = toPropertyKey(eventKey);
      const data = eventData == null ? null : frozenCopy(eventData);
      if (key in registry) {
        // There is already a set of callbacks for the provided key.
        for (const callback of registry[key]) {
          callback.call(this, data);
        }
      } else {
        // There is not already a set of callbacks for the provided
        // key. No callback has been registered.
        /* do nothing */
      }
      return this;
    }
  }

  /**
   * Registers the provided callback function with the provided event
   * key, and returns this object.
   *
   * If the provided callback function has already been registered on
   * this object, its order is not changed.
   */
  register(eventKey, callbackFn) {
    if (!isObject(this)) {
      // This is not an object.
      throw new TypeError(
        "Baron: Callbacks can only be registered on objects.",
      );
    } else {
      // This is an object.
      const registry = callbackRegistryForObject(this);
      const key = toPropertyKey(eventKey);
      if (typeof callbackFn == "function") {
        // The callback function is callable.
        if (key in registry) {
          // There is already a set of callbacks for the provided key.
          registry[key].add(callbackFn);
        } else {
          // There is not already a set of callbacks for the provided
          // key; one must be created.
          const callbacksForKey = new Set();
          callbacksForKey.add(callbackFn);
          registry[key] = callbacksForKey;
        }
        return this;
      } else {
        // The callback function is not callable.
        throw new TypeError("Baron: Callback not callable.");
      }
    }
  }

  /**
   * Unregisters the provided callback function with the provided event
   * key, and returns this object.
   */
  unregister(eventKey, callbackFn) {
    if (!isObject(this)) {
      // This is not an object.
      throw new TypeError(
        "Baron: Callbacks can only be unregistered on objects.",
      );
    } else {
      // This is an object.
      const registry = callbackRegistryForObject(this);
      const key = toPropertyKey(eventKey);
      if (typeof callbackFn == "function") {
        // The callback function is callable.
        if (key in registry) {
          // There is already a set of callbacks for the provided key.
          const callbacksForKey = registry[key];
          callbacksForKey.delete(callbackFn);
          if (callbacksForKey.size == 0) {
            // The set of callbacks is now empty.
            delete registry[key];
          } else {
            // The set of callbacks is non·empty.
            /* do nothing */
          }
        } else {
          // There is not already a set of callbacks for the provided
          // key. No callback has been registered.
          /* do nothing */
        }
        return this;
      } else {
        // The callback function is not callable.
        throw new TypeError("Baron: Callback not callable.");
      }
    }
  }
}

/** The finalization registry for Baron instances. */
const baronFinalizationRegistry = new globalThis.FinalizationRegistry(
  $deinitialize,
);

/**
 * A weak map associating documents with their corresponding Baron
 * instance.
 *
 * A `WeakMap` is used for this in order to maintain a single,
 * non·exposed source of truth for whether a document actually has a
 * Baron instance attached or not.
 */
const baronsForDocuments = new WeakMap();

/**
 * Returns the callback registry for the provided object.
 */
const callbackRegistryForObject = (() => {
  /**
   * A weak map associating objects with their corresponding callback
   * registries.
   *
   * This is used to make the event handling of Baron instances
   * generic, so that one can in fact listen for events on other kinds
   * of objects.
   */
  const callbackRegistriesForObjects = new WeakMap();

  return ($) => {
    if (callbackRegistriesForObjects.has($)) {
      // There is an existing callback registry for the provided object.
      return callbackRegistriesForObjects.get($);
    } else {
      // There is not an existing callback registry for the provided
      // object; one needs to be made.
      const result = Object.create(null);
      callbackRegistriesForObjects.set($, result);
      return result;
    }
  };
})();

//deno-lint-ignore no-unused-labels
finalSetup: {
  // Somebody might nefariously change the Baron prototype, so we need
  // to grab this function now.
  const verifyBaron = Function.prototype.call.bind(
    Object.getOwnPropertyDescriptor(Baron.prototype, "baron").get,
  );

  // Do not allow simple redefinition of important static properties of
  // the Baron constructor; some of these can still be redefined via
  // `Object.defineProperty`.
  Object.defineProperties(Baron, {
    deinitializers: { writable: false },
    initializers: { writable: false },
    initialized: { configurable: false, writable: false },
    symbol: { configurable: false, writable: false },
  });

  if ("document" in globalThis) {
    // Define `Baron.symbol` on the Document prototype to get the Baron
    // instance associated with a given document, if one exists.
    //
    // This uses `document` and a simple `in` check instead of
    // `Document.prototype` to increase robustness in environments
    // which are polyfilling only part of the D·O·M.
    Object.defineProperty(
      Object.getPrototypeOf(globalThis.document),
      Baron.symbol,
      {
        configurable: true,
        enumerable: false,

        /**
         * Returns the current Baron instance associated with this
         * document, or undefined if none exists.
         */
        get() {
          return baronsForDocuments.get(this);
        },

        /**
         * Sets the current Baron instance associated with this
         * document to be the provided value (must be a Baron), or
         * simply unassociates it if the provided value is undefined.
         *
         * This will throw an error if the `context.document` of the
         * provided Baron instance does not match this document.
         */
        set($) {
          if ($ === undefined) {
            baronsForDocuments.delete(this);
          } else {
            // Will throw a TypeError if the provided value was not
            // constructed using the Baron constructor.
            const document = verifyBaron($).context.document;
            if (document !== this) {
              // The context document of the provided value is not this
              // document.
              throw new TypeError(
                "Baron: Cannot associate document with Baron instance which does not point to it in its context.",
              );
            } else {
              // The provided value is a Baron instance which points to
              // this document in its context.
              baronsForDocuments.set(this, $);
            }
          }
        },
      },
    );
  }
}
