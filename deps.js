// 🎩🐈 Baron ∷ deps.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

export {
  frozenCopy,
  isObject,
  toPropertyKey,
} from "https://gitlab.com/kibigo/Pisces/-/raw/9c5f9a7279039f876b5d27eacc79d4b5081c774d/mod.js";
