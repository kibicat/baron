// 🎩🐈 Baron ∷ custom-elements.test.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import Baron from "./baron.js";
import "./custom-elements.js";
import { assert, assertStrictEquals } from "./dev-deps.js";

const store = {
  calls: 0,
  registry: {},
  extends: {},
};
globalThis.customElements = {
  define(name, definition, config) {
    store.registry[name] = definition;
    store.extends[name] = config?.extends ?? null;
    ++store.calls;
  },
};
Baron[Baron.customElements] = {
  bad: Symbol(),
  "still-Bad": Symbol(),
  "ŋo-good": Symbol(),
  "o-k-": Symbol(),
  "extends-something": { [Baron.customElementExtends]: "something" },
};
new Baron();

Deno.test({
  name: "Custom elements are defined on initialization.",
  fn: () => {
    assertStrictEquals(
      store.registry["o-k-"],
      Baron[Baron.customElements]["o-k-"],
    );
    assertStrictEquals(
      store.registry["extends-something"],
      Baron[Baron.customElements]["extends-something"],
    );
  },
});

Deno.test({
  name: "Custom element extensions are properly defined.",
  fn: () => {
    assertStrictEquals(store.extends["o-k-"], null);
    assertStrictEquals(
      store.extends["extends-something"],
      "something",
    );
  },
});

Deno.test({
  name: "Invalid custom element names are not defined.",
  fn: () => {
    assert(!("bad" in store.registry));
    assert(!("still-Bad" in store.registry));
    assert(!("ŋo-good" in store.registry));
  },
});

Deno.test({
  name: "The custom elements object is frozen after initialization.",
  fn: () => {
    const desc = Object.getOwnPropertyDescriptor(
      Baron,
      Baron.customElements,
    );
    assert(!desc.configurable);
    assert(!desc.writable);
    assert(Object.isFrozen(desc.value));
  },
});

Deno.test({
  name: "Elements are only registered once.",
  fn: () => {
    assertStrictEquals(store.calls, 2);
    new Baron();
    assertStrictEquals(store.calls, 2);
  },
});
